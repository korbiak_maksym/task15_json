package com.korbiak.controller;

import com.korbiak.model.Model;

import java.io.File;


public class Controller {

    private Model model;

    public Controller() {
        this.model = new Model();
    }

    public String readByGoogle() {
        return model.readByGoogle();
    }

    public String readByJackson() {
        return model.readByJackson();
    }

    public String writeByGoogle() {
        return model.writeByGoogle();
    }

    public String writeByJackson() {
        return model.writeByJackson();
    }


    public String print() {
        return model.print();
    }

    public void sort() {
        model.sort();
    }
}
