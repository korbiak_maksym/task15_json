package com.korbiak.model.parser;

import com.google.gson.Gson;
import com.korbiak.model.object.DeviceList;

import java.io.*;

public class Google {

    public DeviceList read(File jsonFile) {
        Gson gson = new Gson();
        try (FileReader fileReader = new FileReader(jsonFile);
             Reader reader = new BufferedReader(fileReader)) {
            return gson.fromJson(reader, DeviceList.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeJSON(DeviceList list, File jsonFile ) {
        Gson gson = new Gson();

        try (FileWriter fileWriter = new FileWriter(jsonFile)) {
            gson.toJson(list, list.getClass(), fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
