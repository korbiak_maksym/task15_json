package com.korbiak.model.parser;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.google.gson.Gson;
import com.korbiak.model.object.Device;
import com.korbiak.model.object.DeviceList;

import java.io.*;

public class Jackson {

    ObjectMapper objectMapper;

    public Jackson() {
        this.objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setVisibility(VisibilityChecker.
                Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
    }


    public DeviceList read(File jsonFile) {
        try {
            return objectMapper.readValue(jsonFile, DeviceList.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void writeJSON(DeviceList list, File jsonFile) {
        try {
            objectMapper.writeValue(jsonFile, list);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
