package com.korbiak.model;

import java.io.File;
import java.io.FileNotFoundException;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileReader;


public class Validator {

    public boolean isValid(File jsonFile, File schemaFile) throws FileNotFoundException {

        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(new FileReader(schemaFile)));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(new FileReader(jsonFile)));

        Schema schema = SchemaLoader.load(jsonSchema);

        try {
            schema.validate(jsonSubject);
        } catch (ValidationException e) {
            return false;
        }
        return true;
    }
}
