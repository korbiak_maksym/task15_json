package com.korbiak.model.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeviceList {

    private List<Device> device;

    public DeviceList() {
        this.device = new ArrayList<>();
    }

    public List<Device> getDeviceList() {
        return device;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.device = deviceList;
    }

    public void sort(){
        Collections.sort(device);
    }
}
