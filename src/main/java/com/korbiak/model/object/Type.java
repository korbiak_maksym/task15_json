package com.korbiak.model.object;

public class Type {
    private Boolean peripheral;
    private int energyConsumption;
    private Boolean cooler;
    private String group;
    private Port ports;

    public Type(){
        ports = new Port();
    }
    public Type(Boolean peripheral, int energyConsumption,
                Boolean cooler, String group, boolean COM, boolean USB, boolean LPT) {
        this.peripheral = peripheral;
        this.energyConsumption = energyConsumption;
        this.cooler = cooler;
        this.group = group;
        this.ports = new Port(COM, USB, LPT);
    }

    public Boolean getPeripheral() {
        return peripheral;
    }

    public void setPeripheral(Boolean peripheral) {
        this.peripheral = peripheral;
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public Boolean getCooler() {
        return cooler;
    }

    public void setCooler(Boolean cooler) {
        this.cooler = cooler;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Port getPorts() {
        return ports;
    }

    public void setPorts(Port ports) {
        this.ports = ports;
    }

    public void setCOM(boolean COM) {
        ports.setCOM(COM);
    }

    public void setUSB(boolean USB) {
        ports.setUSB(USB);
    }

    public void setLPT(boolean LPT) {
        ports.setLPT(LPT);
    }

    public boolean getCOM() {
        return  ports.isCOM();
    }

    public boolean getUSB() {
        return  ports.isUSB();
    }

    public boolean getLPT() {
        return  ports.isLPT();
    }

    @Override
    public String toString() {
        return "Type{" +
                "peripheral=" + peripheral +
                ", energyConsumption=" + energyConsumption +
                ", cooler=" + cooler +
                ", group='" + group + '\'' +
                ", ports=" + ports +
                '}';
    }
}
