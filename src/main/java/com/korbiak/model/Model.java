package com.korbiak.model;

import com.korbiak.model.object.Device;
import com.korbiak.model.object.DeviceList;
import com.korbiak.model.parser.Google;
import com.korbiak.model.parser.Jackson;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class Model {
    Google google;
    Jackson jackson;
    DeviceList deviceList;
    Validator validator;
    File file;
    File schemaFile;

    public Model() {
        this.google = new Google();
        this.jackson = new Jackson();
        this.deviceList = new DeviceList();
        this.validator = new Validator();
        this.file = new File("src/main/resources/devices.json");
        this.schemaFile = new File("src/main/resources/schema.json");
    }

    public String readByGoogle() {
        try {
            if (validator.isValid(file, schemaFile))
                deviceList = google.read(file);
            else
                return "Valid err";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "Reade json";
    }

    public String readByJackson() {
        try {
            if (validator.isValid(file, schemaFile))
                deviceList = jackson.read(file);
            else
                return "Valid err";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "Reade json";
    }

    public String writeByGoogle() {
        try {
            if (validator.isValid(file, schemaFile) &&
                    !deviceList.getDeviceList().isEmpty())
                google.writeJSON(deviceList, file);
            else
                return "Valid err";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "Wrote json";
    }

    public String writeByJackson() {
        jackson.writeJSON(deviceList, file);
        return "Wrote json";
    }


    public String print() {
        StringBuilder answer = new StringBuilder();
        List<Device> list = deviceList.getDeviceList();
        for (Device device : list) {
            answer.append(device).append("\n");
        }
        return answer.toString();
    }


    public void sort() {
        deviceList.sort();
    }

}
