package com.korbiak.view;

import com.korbiak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(View.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print");
        menu.put("2", "2 - Sort");
        menu.put("3", "3 - Read by Google");
        menu.put("4", "4 - Write by Google");
        menu.put("5", "5 - Read by Jackson");
        menu.put("6", "6 - Write by Jackson");
        menu.put("Q", "Q - exit");
    }

    public View() {

        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getEx1);
        methodsMenu.put("2", this::getEx2);
        methodsMenu.put("3", this::getEx3);
        methodsMenu.put("4", this::getEx4);
        methodsMenu.put("5", this::getEx5);
        methodsMenu.put("6", this::getEx6);
    }

    private void getEx6() {
        logger.info(controller.writeByJackson());
    }

    private void getEx5() {
        logger.info(controller.readByJackson());
    }

    private void getEx4() {
        logger.info(controller.writeByGoogle());
    }

    private void getEx3() {
        logger.info(controller.readByGoogle());
    }

    private void getEx2() {
        controller.sort();
    }

    private void getEx1() {
        logger.info(controller.print());
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).getCom();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }
}
